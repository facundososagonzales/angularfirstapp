import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'src/Models/Subscription';

@Component({
  selector: 'app-search-card',
  templateUrl: './search-card.component.html',
  styleUrls: ['./search-card.component.css']
})
export class SearchCardComponent implements OnInit {

  @Input()
  sub!: Subscription;

  constructor() {}

  ngOnInit(): void {
  }

}
